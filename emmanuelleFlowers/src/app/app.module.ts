import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';
import { FlowerslistComponent } from './flowerslist/flowerslist.component';
import { ListafloresComponent } from './listaflores/listaflores.component';

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    FlowerslistComponent,
    ListafloresComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


