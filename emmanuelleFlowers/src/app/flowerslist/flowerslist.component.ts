import { Component, OnInit, Input, HostBinding } from '@angular/core'; //aqui se pone todas las clases odecoradores de angular o cualquier otro paquete
//se registra Input, para señalar un nuevo decorador
import { ListarFlores } from '../models/lista-flores.models';

@Component({
  selector: 'app-flowerslist',
  templateUrl: './flowerslist.component.html',
  styleUrls: ['./flowerslist.component.css']
})
export class FlowerslistComponent implements OnInit {
  @Input() flores: ListarFlores; //aqui se define el tipo de variable
  
  @HostBinding('attr.class') cssClass='col-md-4';
  constructor() { 

    //this.nombre='nombre por defecto';


  }
  
  ngOnInit(): void {
  }

}
