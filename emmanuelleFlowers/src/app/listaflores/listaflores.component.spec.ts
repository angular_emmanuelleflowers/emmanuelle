import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListafloresComponent } from './listaflores.component';

describe('ListafloresComponent', () => {
  let component: ListafloresComponent;
  let fixture: ComponentFixture<ListafloresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListafloresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListafloresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
