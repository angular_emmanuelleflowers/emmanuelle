import { Component, OnInit } from '@angular/core';
import { ListarFlores } from '../models/lista-flores.models';

@Component({
  selector: 'app-listaflores',
  templateUrl: './listaflores.component.html',
  styleUrls: ['./listaflores.component.css']
})
export class ListafloresComponent implements OnInit {
  flores:ListarFlores[]; //se indica que es array
  constructor() { 
    this.flores=[];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    this.flores.push(new ListarFlores(nombre, url));
    console.log(this.flores);
    return false; //este retorno no es para recargar la página
  }
}
